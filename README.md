# Release test

project for testing gitlabs "release" concpet

We need an [API token](https://gitlab.com/help/api/releases/index.md) for this, and the project id.


# Setting up

Reusing the variables names that we will encounter when doing CI.

```
export APITOKEN=asdfasdfASDFa
export CI_PROJECT_URL=https://gitlab.com/me/myproject
export CI_PROJECT_ID=12345
export CI_COMMIT_TAG=some_tag
export CI_API_V4_URL=https://gitlab.com/api/v4
export RELEASES_API_PATH=/projects/$CI_PROJECT_ID/releases
export UPLOAD_API_PATH=/projects/$CI_PROJECT_ID/uploads
```

## A note on `curl`

when doing the API calls, when you get json back with "message" in it. There is an error.

Other wise you need to look at the return code. `curl`only shows it if `-v`is added.

# Working with releases

Releases are usually associated with tags and may also hold build artifacts.

## Checking release

```
curl -v --header "PRIVATE-TOKEN: $APITOKEN" "$CI_API_V4_URL$RELEASES_API_PATH" | jq ""
```

`jq` is used to prettify


## Creating releases

create a tag and push it

```
git tag $CI_COMMIT_TAG
git push --tags
```

see it in [gitlab](https://gitlab.com/moozer/release-test/-/tags)


create a release.

I find it a lot less error prone and more debuggable to make a temporary file

Version without assets
```
cat <<EOF > release.json
{
  "name": "CI_COMMIT_TAG",
  "tag_name": "$CI_COMMIT_TAG",
  "description": "some description"
}  

EOF
```

Version with assets
```
cat <<EOF > release.json
{
  "name": "$CI_COMMIT_TAG",
  "tag_name": "$CI_COMMIT_TAG",
  "description": "some description",
  "assets": {
    "links":  [
      {
        "name": "google link",
        "url": "https://google.com"
      }
    ]
  }
}  

EOF
```

some comments:
* zipped source will be added automatically
* `cat release.json | jq` will tell you if there is a json err (which in turn will give a `400 Bad request`)


And apply it
```
curl -v --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $APITOKEN" \
     --data @release.json \
     --request POST "$CI_API_V4_URL$RELEASES_API_PATH" | jq ""

```

Updating releases works the same way, see the [docs](https://gitlab.com/help/api/releases/index.md#update-a-release)


## Deleting a release

Add a release that we will delete
```
git tag ${CI_COMMIT_TAG}-deleteme
git push --tags
cat <<EOF > release.json
{
  "name": "${CI_COMMIT_TAG}-deleteme",
  "tag_name": "${CI_COMMIT_TAG}-deleteme",
  "description": "some description"
}  

EOF

curl -v --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $APITOKEN" \
     --data @release.json \
     --request POST "$CI_API_V4_URL$RELEASES_API_PATH" | jq ""

```

and delete it again
```
curl -v --header "PRIVATE-TOKEN: $APITOKEN" --request DELETE "$CI_API_V4_URL$RELEASES_API_PATH/${CI_COMMIT_TAG}-deleteme" | jq ""
```


### "message": "Ref is not specified"

wrong tag. Is `CI_COMMIT_TAG` set properly and did it get expanded properly.

###   "message": "404 Project Not Found"

Check the project id, e.g. by [`Checking release`](#checking-release)

# Adding more links

Links are the extra files and such associated with releases.

There is a deeper API end point for that. See the [gitlab docs](https://gitlab.com/help/api/releases/links.md)

## Checking links

To get the list
```
curl -v --header "PRIVATE-TOKEN: $APITOKEN" "$CI_API_V4_URL$RELEASES_API_PATH/$CI_COMMIT_TAG/assets/links" | jq ""
```

This also give the links id, which you might need.

## Adding links

Create the json

```
cat <<EOF > links.json
{
  "name": "google link #2",
  "url": "https://google.com"
}

EOF
```
Only one at a time may be updated

and apply it
```
curl -v --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $APITOKEN" \
     --data @links.json \
     --request POST "$CI_API_V4_URL$RELEASES_API_PATH/$CI_COMMIT_TAG/assets/links" | jq ""
```

# uploading artifact

I find that linking to build artifact is error prone, and it is better to `upload` the artifact, that the release should point to. This is also the recommandations I see online, like e.g. [here] (It makes for nicer URLs too)

We will use some junk data for the example
```
dd if=/dev/urandom of=data bs=1k count=10
sha256sum data > data.sha256
```

Upload files
```
curl --request POST --header "PRIVATE-TOKEN: $APITOKEN" --form "file=@data" $CI_API_V4_URL$UPLOAD_API_PATH > upload_receipt.json
cat upload_receipt.json | jq
```

This returns json with the url for the uploaded file. Notice that there a currently no way of managing these files on gitlab.com or through the api, so you cannot retrieve lost urls easily, see e.g. [stackoverflow](https://stackoverflow.com/questions/38228508/where-can-i-manage-files-uploaded-to-wiki-of-a-gitlab-com-project) or [this](https://gitlab.com/gitlab-org/gitlab/issues/16229) gitlab issue.

You can then download the uploaded gitlab.com.
```
export UPLOAD_URL=`cat upload_receipt.json | jq -r '.url'`
wget $CI_PROJECT_URL$UPLOAD_URL
```

(And you can check sha256 sums to convince yourself that is is working)

Also notice that the uploads are only available to `guests` or higher privileges. This will probably be an issue for non-public projects.

# Docs

* [gitlab release docs](https://gitlab.com/help/user/project/releases/index)
* [gitlab relase api](https://gitlab.com/help/api/releases/index.md)
* [gitlab upload api](https://docs.gitlab.com/ee/api/projects.html#upload-a-file)
